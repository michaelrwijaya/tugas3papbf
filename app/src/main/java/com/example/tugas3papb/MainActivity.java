package com.example.tugas3papb;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    ImageView _Kumis, _Janggut, _Hair, _Eyebrow;
    CheckBox _cekJanggut, _cekAlis, _cekRambut, _cekKumis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        _Kumis = findViewById(R.id.Kumis);
        _Janggut = findViewById(R.id.Janggut);
        _Eyebrow = findViewById(R.id.Alis);
        _Hair = findViewById(R.id.Rambut);
        _cekAlis = findViewById(R.id.boxAlis);
        _cekRambut = findViewById(R.id.boxRambut);
        _cekJanggut = findViewById(R.id.boxjanggut);
        _cekKumis = findViewById(R.id.boxKumis);

        _cekRambut.setOnClickListener(this);
        _cekAlis.setOnClickListener(this);
        _cekKumis.setOnClickListener(this);
        _cekJanggut.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (_cekAlis.getId() == v.getId()) {
            if (_cekAlis.isChecked()) {
                _Eyebrow.setVisibility(View.VISIBLE);
            } else {
                _Eyebrow.setVisibility(View.INVISIBLE);
            }
        }

        if (_cekKumis.getId() == v.getId()) {
            if (_cekKumis.isChecked()) {
                _Kumis.setVisibility(View.VISIBLE);
            } else {
                _Kumis.setVisibility(View.INVISIBLE);
            }
        }

        if (_cekRambut.getId() == v.getId()) {
            if (_cekRambut.isChecked()) {
                _Hair.setVisibility(View.VISIBLE);
            } else {
                _Hair.setVisibility(View.INVISIBLE);
            }
        }

        if (_cekJanggut.getId() == v.getId()) {
            if (_cekJanggut.isChecked()) {
                _Janggut.setVisibility(View.VISIBLE);
            } else {
                _Janggut.setVisibility(View.INVISIBLE);
            }
        }
    }
}